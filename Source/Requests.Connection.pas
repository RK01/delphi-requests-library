unit Requests.Connection;

interface

uses
  Classes, Requests.Core;

type
  IConnection = interface
    function Get(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Post(AUrl, AContent: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;
    function Post(AUrl: string; AContent: TStringList; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;
    function Post(AUrl: string; AContent: IRequestsDictionary; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;

    function Delete(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Head(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Options(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;
  end;

  IConnectionFactory = interface
    function CreateConnection(): IConnection;
  end;


implementation

end.
