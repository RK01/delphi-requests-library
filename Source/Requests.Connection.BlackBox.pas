unit Requests.Connection.BlackBox;

interface

uses
  Requests, Requests.Connection;

type
  TConnectionBlackBoxFactory = class(TInterfacedObject, IConnectionFactory)
  public
    function CreateConnection(): IConnection;
  end;


function Factory: IConnectionFactory;


implementation

uses
  Classes, SysUtils, Windows, TypInfo, Spring, Generics.Collections,
  Requests.Core, OtlTask, SBHTTPSClient, SBX509;


type
  TConnectionBlackBox = class(TInterfacedObject, IConnection)
  private
    FReceivingData: TRTLCriticalSection;
    FClient: TElHTTPSClient;

    FResponse: string;
    FResponseHeader: string;

    procedure OnReceivingHeaders(Sender: TObject; Headers: TStringList);
    procedure OnCertificateValidate(Sender: TObject; Certificate: TElX509Certificate; var Validate: Boolean);
    procedure OnDocumentBegin(Sender: TObject);
    procedure OnData(Sender: TObject; Buffer: Pointer; Size: Integer);
    procedure OnDocumentEnd(Sender: TObject);
    procedure OnSendData(Sender: TObject; Buffer: Pointer; Size: Integer);
    procedure OnError(Sender: TObject; ErrorCode: Integer; Fatal, Remote: Boolean);
    procedure OnPreparedHeaders(Sender: TObject; Headers: TStringList);

    procedure CopyRequestParams(AFrom: IHeaders; ADestination: TElHTTPRequestParams);
  public
    constructor Create;
    destructor Destroy; override;

    function Request(AMethod: TMethod; AUrl, AContent: string;
      AContentStrings: TStringList; AContentDict: IRequestsDictionary;
      var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Get(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Post(AUrl, AContent: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;
    function Post(AUrl: string; AContent: TStringList; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;
    function Post(AUrl: string; AContent: IRequestsDictionary; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer; overload;

    function Delete(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Head(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;

    function Options(AUrl: string; var AResponse, AResponseHeader: string;
      ARequestHeaders: IHeaders): Integer;
  end;



var
  FFactory: IConnectionFactory;

function Factory: IConnectionFactory;
begin
  if FFactory=nil then
    FFactory := TConnectionBlackBoxFactory.Create;

  Result := FFactory;
end;

{ TConnectionBlackBoxFactory }

function TConnectionBlackBoxFactory.CreateConnection: IConnection;
begin
  Result := TConnectionBlackBox.Create;
end;

{ TConnectionBlackBox }

procedure TConnectionBlackBox.CopyRequestParams(AFrom: IHeaders; ADestination: TElHTTPRequestParams);
begin
  AFrom.Enumerable.ForEach(
    procedure(const Pair: Tpair<string, TValue>)
    var
      PropInfo: PPropInfo;
    begin
      PropInfo := GetPropInfo(ADestination.ClassInfo, Pair.Key);
      if Assigned(PropInfo) then
        SetStrProp(ADestination, PropInfo, Pair.Value.ToString)
      else
        ADestination.CustomHeaders.Values[Pair.Key] := Pair.Value.ToString;
    end
  );

//  PropInfo := GetPropInfo(FHeader.ClassInfo, AName);
//  if Assigned(PropInfo) then
//    SetStrProp(FHeader, PropInfo, AValue)
//  else
//    FHeader.CustomHeaders.Values[AName] := AValue;

//  ADestination.Accept := AFrom.Get('Accept').AsString;
//  ADestination.AcceptCharset := AFrom.Get('AcceptCharset').AsString;
//  ADestination.AcceptLanguage := AFrom.Get('AcceptLanguage').AsString;
//  ADestination.Authorization := AFrom.Get('Authorization').AsString;
////  ADestination.AcceptRanges := AFrom.Get('AcceptRanges');
//
//  ADestination.Connection := AFrom.Get('Connection').AsString;
//  ADestination.ContentType := AFrom.Get('ContentType').AsString;
//  ADestination.Cookie := AFrom.Get('Cookie').AsString;
//  ADestination.Cookies.Text := AFrom.Get('Cookies.Text').AsString;
//  ADestination.CustomHeaders.Text := AFrom.Get('CustomHeaders.Text').AsString;
//
//  ADestination.From := AFrom.Get('From').AsString;
//  ADestination.Host := AFrom.Get('Host').AsString;
//
//  ADestination.IfMatch := AFrom.Get('IfMatch').AsString;
////  ADestination.IfModifiedSince := AFrom.Get('IfModifiedSince');
//  ADestination.IfNoneMatch := AFrom.Get('IfNoneMatch').AsString;
////  ADestination.IfUnmodifiedSince := AFrom.Get('IfUnmodifiedSince');
//
//  ADestination.Password := AFrom.Get('Password').AsString;
//  ADestination.Username := AFrom.Get('Username').AsString;
//  ADestination.Referer := AFrom.Get('Referer').AsString;
//  ADestination.UserAgent := AFrom.Get('UserAgent').AsString;
end;

procedure TConnectionBlackBox.OnData(Sender: TObject; Buffer: Pointer;
  Size: Integer);
var
  TmpS: UTF8String;
begin
  SetLength(TmpS, Size);
  Move(PAnsiString(Buffer)^, TmpS[1], Length(TmpS));
  FResponse := FResponse + TmpS;
end;

procedure TConnectionBlackBox.OnDocumentEnd(Sender: TObject);
begin
  LeaveCriticalSection(FReceivingData);
end;

procedure TConnectionBlackBox.OnDocumentBegin(Sender: TObject);
begin
//  FResponse := '';
//  FResponseHeader := '';
end;

procedure TConnectionBlackBox.OnError(Sender: TObject; ErrorCode: Integer;
  Fatal, Remote: Boolean);
begin
  LeaveCriticalSection(FReceivingData);
end;

procedure TConnectionBlackBox.OnPreparedHeaders(Sender: TObject;
  Headers: TStringList);
begin
  OutputDebugString(PChar(Headers.Text));
end;

procedure TConnectionBlackBox.OnReceivingHeaders(Sender: TObject;
  Headers: TStringList);
begin
  FResponseHeader := Headers.Text;
end;

procedure TConnectionBlackBox.OnSendData(Sender: TObject; Buffer: Pointer;
  Size: Integer);
begin
  EnterCriticalSection(FReceivingData);
end;

constructor TConnectionBlackBox.Create;
begin
  InitializeCriticalSection(FReceivingData);

  FClient := TElHTTPSClient.Create(nil);
  FClient.SocketTimeout := 0;
  FClient.KeepAlivePolicy := kapKeepAlivesDisabled;

  FClient.OnReceivingHeaders := OnReceivingHeaders;
  FClient.OnCertificateValidate := OnCertificateValidate;
  FClient.OnData := OnData;
  FClient.OnSendData := OnSendData;
  FClient.OnDocumentBegin := OnDocumentBegin;
  FClient.OnDocumentEnd := OnDocumentEnd;
  FClient.OnError := OnError;
  FClient.OnPreparedHeaders := OnPreparedHeaders;
end;

destructor TConnectionBlackBox.Destroy;
begin
  FClient.Free;
  DeleteCriticalSection(FReceivingData);
  inherited;
end;

procedure TConnectionBlackBox.OnCertificateValidate(Sender: TObject;
  Certificate: TElX509Certificate; var Validate: Boolean);
//var
//  Validity : TSBCertificateValidity;
//  Reason: TSBCertificateValidityReason;
begin
//  if (Certificate.Chain = nil) or Certificate.Chain.Certificates[0].Equals(Certificate) then
//  begin
//	// For proper CRL and OCSP validation please read instructions in
//	// description of ElX509CertificateValidator class in the help file
//    CertificateValidator.ValidateForSSL(Certificate, HTTPSClient.RemoteHost, HTTPSClient.RemoteIP, hrServer, nil, true, Now, Validity, Reason);
//    Validate := Validity = cvOk;
//  end
//  else
  Validate := True;
end;

function TConnectionBlackBox.Request(AMethod: TMethod; AUrl, AContent: string;
  AContentStrings: TStringList; AContentDict: IRequestsDictionary;
  var AResponse, AResponseHeader: string; ARequestHeaders: IHeaders): Integer;
var
  TmpStrings: TStringList;
begin
  Result := 502;

  try
    try
      FResponse := '';
      FResponseHeader := '';
      CopyRequestParams(ARequestHeaders, FClient.RequestParameters);

      case AMethod of
        rmGET:
          Result := FClient.Get(AUrl);

        rmPOST:
          begin
            if AContent <> '' then
              Result := FClient.Post(AUrl, AContent)
            else if AContentStrings<>nil then
              Result := FClient.PostWebForm(AUrl, AContentStrings)
            else if AContentDict<>nil then
            begin
              TmpStrings := TStringList.Create;
              try
                AContentDict.Enumerable.ForEach(
                  procedure(const APair: TPair<string, TValue>)
                  begin
                    TmpStrings.Values[APair.Key] := APair.Value.ToString;
                  end
                );
                Result := FClient.PostWebForm(AUrl, TmpStrings);
              finally
                TmpStrings.Free;
              end;
            end;
          end;

        rmDELETE:
          Result := FClient.Delete(AUrl);

        rmPUT:
          Assert(True, 'PUT method not implemented');

        rmPATCH:
          Assert(True, 'PATCH method not implemented');

        rmHEAD:
          Result := FClient.Head(AUrl);

        rmOPTIONS:
          Result := FClient.Options(AUrl);

      end;

    finally
      FClient.Close(true);
    end;

    EnterCriticalSection(FReceivingData);
    AResponse := FResponse;
    AResponseHeader := FResponseHeader;
    LeaveCriticalSection(FReceivingData);
  except
    on E : Exception do
    begin
      LeaveCriticalSection(FReceivingData);
      raise;
    end;
  end;
end;

function TConnectionBlackBox.Get(AUrl: string;
  var AResponse, AResponseHeader: string;
  ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmGET, AUrl, '', nil, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Post(AUrl, AContent: string;
  var AResponse, AResponseHeader: string;
  ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmPOST, AUrl, AContent, nil, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Post(AUrl: string; AContent: TStringList;
  var AResponse, AResponseHeader: string;
  ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmPOST, AUrl, '', AContent, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Post(AUrl: string;
  AContent: IRequestsDictionary; var AResponse, AResponseHeader: string;
  ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmPOST, AUrl, '', nil, AContent, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Delete(AUrl: string; var AResponse,
  AResponseHeader: string; ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmDELETE, AUrl, '', nil, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Head(AUrl: string; var AResponse,
  AResponseHeader: string; ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmHEAD, AUrl, '', nil, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

function TConnectionBlackBox.Options(AUrl: string; var AResponse,
  AResponseHeader: string; ARequestHeaders: IHeaders): Integer;
begin
  Result := Request(rmOPTIONS, AUrl, '', nil, nil, AResponse, AResponseHeader, ARequestHeaders);
end;

end.
