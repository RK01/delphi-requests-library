unit Requests.Core;

interface

uses
  Classes, SysUtils, OtlParallel, Spring, Spring.Collections, Generics.Collections;

type
  TMethod = (rmGET, rmPOST, rmDELETE, rmPUT, rmPATCH, rmHEAD, rmOPTIONS);

  ERequestsException = class(Exception);

  IRequestsDictionary = interface
    function GetHeaderByName(AName: string; AValue: TValue): IRequestsDictionary;
    property HeaderByName[AName: string; AValue: TValue]: IRequestsDictionary read GetHeaderByName; default;

    function Get(AKey: string): TValue;
    function GetEnumerable: IEnumerable<TPair<string,TValue>>;
    property Enumerable: IEnumerable<TPair<string,TValue>> read GetEnumerable;
  end;

  IParameters = interface(IRequestsDictionary)
    function GetHeaderByName(AName: string; AValue: TValue): IParameters;
    property HeaderByName[AName: string; AValue: TValue]: IParameters read GetHeaderByName; default;

    function AsString: string;
  end;
  IHeaders = IRequestsDictionary;
  ICookies = IRequestsDictionary;


  IResponse = interface
    function GetStatusCode: Integer;
    function GetStream: TStream;
    function GetText: string;
    function GetURL: string;
    function GetElapsed: LongInt;
    function GetHeaders: IHeaders;
    function GetCookies: ICookies;

    property StatusCode: Integer read GetStatusCode;
    property Stream: TStream read GetStream;
    property Text: string read GetText;
    property URL: string read GetURL;
    property Elapsed: LongInt read GetElapsed;
    property Headers: IHeaders read GetHeaders;
    property Cookies: ICookies read GetCookies;
  end;

  IResponseFuture = IOmniFuture<IResponse>;
  TResponseReference = reference to procedure(AResponse: IResponse);
  TResponseCallback = procedure(AResponse: IResponse);
  TResponseMethod = procedure(AResponse: IResponse) of object;


implementation

end.
