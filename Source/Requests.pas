unit Requests;

interface

uses
  SysUtils, Classes, Spring, Generics.Collections, Requests.Core, Requests.Connection;

type
  TRequests = class
  private
    class var FDefaultHeader: IHeaders;
    class var FDefaultConnectionFactory: IConnectionFactory;
  protected
    class constructor ClassCreate;
    class destructor ClassDestroy;

    class function GetDefaultHeaders: IHeaders; static;

    // preveri parametra in �e nista nastavljena jima priredi Default vrednosti
    class procedure CheckParametersAndConnectionFactory(var AHeaders: IHeaders;
      var IConnectionFactory: IConnectionFactory);
  public
    { Factory, ki poskrbi za kreiranje objektov, ki dejansko izvajajo Get in Post.
      caHTTPClient tako sploh ni odvisen od Indy, Synapse ali SecureBlackBox komponent.

      caHTTPClientConnectionFactoryBlackBox.Factory dela to z BlackBox komponentami,
      caHTTPClientConnectionFactoryIndy.Factory pa (bo) delal preko Indy komponent. }
    class property DefaultConnectionFactory: IConnectionFactory read FDefaultConnectionFactory write FDefaultConnectionFactory;

    { Privzeti Header, ki se po�ilja pri poizvedbah.
      �e v Get in Post zahtevah ne podamo parametra A}
    class property DefaultHeaders: IHeaders read GetDefaultHeaders;

    class function Request(AMethod: TMethod; AUrl: string;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      AContent: string=''; AFieldsStrings: TStringList=nil; AFieldsDictionary: IRequestsDictionary=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Request(AMethod: TMethod; AUrl: string;
      AResponseReference: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      AContent: string=''; AFieldsStrings: TStringList=nil; AFieldsDictionary: IRequestsDictionary=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;


    class function Get(AUrl: string;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Get(AUrl: string; AResponse: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;

    class function Post(AUrl, AContent: string;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class function Post(AUrl: string; AFields: TStringList;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class function Post(AUrl: string; AFields: IRequestsDictionary;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Post(AUrl, AContent: string; AResult: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;
    class procedure Post(AUrl: string; AFields: TStringList; AResult: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;
    class procedure Post(AUrl: string; AFields: IRequestsDictionary; AResult: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;

    class function Delete(AUrl: string;
      AParams: IParameters;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class function Delete(AUrl: string;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Delete(AUrl: string; AResponse: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;

    class function Head(AUrl: string;
      AParams: IParameters;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class function Head(AUrl: string;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Head(AUrl: string; AResponse: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;

    class function Options(AUrl: string;
      AParams: IParameters;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class function Options(AUrl: string;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture; overload;
    class procedure Options(AUrl: string; AResponse: TResponseReference;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil); overload;

  end;



implementation

uses
  OtlParallel, Requests.Dictionary;

type
  TResponse = class(TInterfacedObject, IResponse)
  private
    FStatusCode: Integer;
    FStream: TStream;
    FText: string;
    FURL: string;
    FHeaders: IHeaders;
    FCookies: ICookies;
  protected
    function GetStatusCode: Integer;
    function GetStream: TStream;
    function GetText: string;
    function GetURL: string;
    function GetElapsed: LongInt;
    function GetHeaders: IHeaders;
    function GetCookies: ICookies;
  public
    constructor Create(AStatusCode: Integer; AText: string; AURL: string);
    destructor Destroy; override;
  end;


{ TcaHTTPClient }

class procedure TRequests.CheckParametersAndConnectionFactory(
  var AHeaders: IHeaders;
  var IConnectionFactory: IConnectionFactory);
begin
  if AHeaders=nil then
    AHeaders := FDefaultHeader;
  if IConnectionFactory=nil then
    IConnectionFactory := FDefaultConnectionFactory;
end;

class constructor TRequests.ClassCreate;
begin
  FDefaultHeader := TDictionary.CreateHeaders;
  FDefaultHeader['UserAgent', 'Delphi Requests Library']
end;

class destructor TRequests.ClassDestroy;
begin
  FDefaultHeader := nil;
end;

class function TRequests.GetDefaultHeaders: IHeaders;
begin
  Result := FDefaultHeader;
end;

class function TRequests.Request(AMethod: TMethod; AUrl: string;
  AParams: IParameters=nil;
  AHeaders: IHeaders=nil;
  AContent: string=''; AFieldsStrings: TStringList=nil; AFieldsDictionary: IRequestsDictionary=nil;
  IConnectionFactory: IConnectionFactory=nil): IResponseFuture;
begin
  CheckParametersAndConnectionFactory(AHeaders, IConnectionFactory);

  if Assigned(AParams) then
    AUrl := AUrl + '?' + AParams.AsString;

  Result := Parallel.Future<IResponse>(
    function: IResponse
    var
      conn: IConnection;
      StatusCode: Integer;
      Response: string;
      ResponseHeader: string;
    begin
      StatusCode := 502;

      conn := IConnectionFactory.CreateConnection;

      case AMethod of
        rmGET:
          StatusCode := conn.Get(AUrl, Response, ResponseHeader, AHeaders);

        rmPOST:
          begin
            if AFieldsStrings<>nil then
              StatusCode := conn.Post(AUrl, AFieldsStrings, Response, ResponseHeader, AHeaders)
            else if AFieldsDictionary<>nil then
              StatusCode := conn.Post(AUrl, AFieldsDictionary, Response, ResponseHeader, AHeaders)
            else
              StatusCode := conn.Post(AUrl, AContent, Response, ResponseHeader, AHeaders)
          end;

        rmDELETE:
          StatusCode := conn.Delete(AUrl, Response, ResponseHeader, AHeaders);

        rmPUT:
          Assert(True, 'PUT method not implemented');

        rmPATCH:
          Assert(True, 'PATCH method not implemented');

        rmHEAD:
          StatusCode := conn.Head(AUrl, Response, ResponseHeader, AHeaders);

        rmOPTIONS:
          StatusCode := conn.Options(AUrl, Response, ResponseHeader, AHeaders);

      end;

      Result := TResponse.Create(StatusCode, Response, AUrl);
    end
  );
end;

class procedure TRequests.Request(AMethod: TMethod; AUrl: string;
  AResponseReference: TResponseReference;
  AParams: IParameters=nil;
  AHeaders: IHeaders=nil;
  AContent: string=''; AFieldsStrings: TStringList=nil; AFieldsDictionary: IRequestsDictionary=nil;
  IConnectionFactory: IConnectionFactory=nil);
var
  ResultFuture: IResponseFuture;
  Response: IResponse;
begin
  Async(procedure
    begin
      ResultFuture := Request(AMethod, AUrl, AParams, AHeaders,
        AContent, AFieldsStrings, AFieldsDictionary, IConnectionFactory);
      Response := ResultFuture.Value;
    end)
  .Await(procedure
    begin
      AResponseReference(Response);
    end)
end;

class function TRequests.Get(AUrl: string;
  AParams: IParameters=nil;
  AHeaders: IHeaders=nil;
  IConnectionFactory: IConnectionFactory=nil): IResponseFuture;
begin
  Result := Request(rmGET, AUrl, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class procedure TRequests.Get(AUrl: string; AResponse: TResponseReference;
  AParams: IParameters=nil;
  AHeaders: IHeaders=nil;
  IConnectionFactory: IConnectionFactory=nil);
begin
  Request(rmGET, AUrl, AResponse, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class function TRequests.Post(AUrl, AContent: string;
      AParams: IParameters=nil;
      AHeaders: IHeaders=nil;
      IConnectionFactory: IConnectionFactory=nil): IResponseFuture;
begin
  Result := Request(rmPOST, AUrl, AParams, AHeaders, AContent, nil, nil, IConnectionFactory);
end;

class function TRequests.Post(AUrl: string; AFields: TStringList;
  AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Request(rmPOST, AUrl, AParams, AHeaders, '', AFields, nil, IConnectionFactory);
end;

class function TRequests.Post(AUrl: string; AFields: IRequestsDictionary;
  AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Request(rmPOST, AUrl, AParams, AHeaders, '', nil, AFields, IConnectionFactory);
end;

class procedure TRequests.Post(AUrl, AContent: string; AResult: TResponseReference;
  AParams: IParameters=nil;
  AHeaders: IHeaders=nil;
  IConnectionFactory: IConnectionFactory=nil);
begin
  Request(rmPOST, AUrl, AResult, AParams, AHeaders, AContent, nil, nil, IConnectionFactory);
end;

class procedure TRequests.Post(AUrl: string; AFields: TStringList;
  AResult: TResponseReference; AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory);
begin
  Request(rmPOST, AUrl, AResult, AParams, AHeaders, '', AFields, nil, IConnectionFactory);
end;

class procedure TRequests.Post(AUrl: string; AFields: IRequestsDictionary;
  AResult: TResponseReference; AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory);
begin
  Request(rmPOST, AUrl, AResult, AParams, AHeaders, '', nil, AFields, IConnectionFactory);
end;

class function TRequests.Delete(AUrl: string; AParams: IParameters;
  AHeaders: IHeaders; IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Request(rmDELETE, AUrl, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class function TRequests.Delete(AUrl: string; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Delete(AUrl, nil, AHeaders, IConnectionFactory);
end;

class procedure TRequests.Delete(AUrl: string; AResponse: TResponseReference;
  AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory);
begin
  Request(rmDELETE, AUrl, AResponse, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class function TRequests.Head(AUrl: string; AParams: IParameters;
  AHeaders: IHeaders; IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Request(rmHEAD, AUrl, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class function TRequests.Head(AUrl: string; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Head(AUrl, nil, AHeaders, IConnectionFactory);
end;

class procedure TRequests.Head(AUrl: string; AResponse: TResponseReference;
  AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory);
begin
  Request(rmHEAD, AUrl, AResponse, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;


class function TRequests.Options(AUrl: string; AParams: IParameters;
  AHeaders: IHeaders; IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Request(rmOPTIONS, AUrl, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

class function TRequests.Options(AUrl: string; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory): IResponseFuture;
begin
  Result := Options(AUrl, nil, AHeaders, IConnectionFactory);
end;

class procedure TRequests.Options(AUrl: string; AResponse: TResponseReference;
  AParams: IParameters; AHeaders: IHeaders;
  IConnectionFactory: IConnectionFactory);
begin
  Request(rmOPTIONS, AUrl, AResponse, AParams, AHeaders, '', nil, nil, IConnectionFactory);
end;

{ TResponse }

constructor TResponse.Create(AStatusCode: Integer; AText: string; AURL: string);
begin
  FHeaders := TDictionary.CreateHeaders;
  FCookies := TDictionary.CreateCookies;
  FStatusCode := AStatusCode;
  FText := AText;
  FURL := AURL;
  FStream := TStream.Create;
end;

destructor TResponse.Destroy;
begin
  FHeaders := nil;
  FCookies := nil;
  FStream.Free;
  inherited;
end;

function TResponse.GetCookies: ICookies;
begin
  Result := FCookies;
end;

function TResponse.GetElapsed: LongInt;
begin

end;

function TResponse.GetHeaders: IHeaders;
begin
  Result := FHeaders;
end;

function TResponse.GetStatusCode: Integer;
begin
  Result := FStatusCode;
end;

function TResponse.GetStream: TStream;
begin
  Result := FStream;
end;

function TResponse.GetText: string;
begin
  Result := FText;
end;

function TResponse.GetURL: string;
begin
  Result := FURL;
end;

end.
