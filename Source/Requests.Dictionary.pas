unit Requests.Dictionary;

interface

uses
  Requests.Core;

type
  TDictionary = class
  public
    class function CreateDictionary: IRequestsDictionary;
    class function CreateHeaders: IHeaders;
    class function CreateCookies: ICookies;
    class function CreateParameters: IParameters;
  end;


implementation

uses
  Generics.Collections, Spring, Spring.Collections;

type
  TRequestsDictionary = class(TInterfacedObject, IRequestsDictionary)
  private
    FDict: IDictionary<string, TValue>;

    function Get(AKey: string): TValue;
    function GetHeaderByName(AName: string; AValue: TValue): IRequestsDictionary;
    function GetEnumerable: IEnumerable<TPair<string,TValue>>;

  public
    constructor Create;
    destructor Destroy; override;

    property HeaderByName[AName: string; AValue: TValue]: IRequestsDictionary read GetHeaderByName; default;
    property Enumerable: IEnumerable<TPair<string,TValue>> read GetEnumerable;
  end;

  TParameters = class(TRequestsDictionary, IParameters)
  private
    function GetHeaderByName(AName: string; AValue: TValue): IParameters;
    function AsString: string;
  end;



class function TDictionary.CreateDictionary: IRequestsDictionary;
begin
  Result := TRequestsDictionary.Create;
end;

class function TDictionary.CreateHeaders: IHeaders;
begin
  Result := TRequestsDictionary.Create;
  Result['UserAgent', 'CADIS HTTP Client'];
end;

class function TDictionary.CreateCookies: ICookies;
begin
  Result := TRequestsDictionary.Create;
end;

class function TDictionary.CreateParameters: IParameters;
begin
  Result := TParameters.Create;
end;



{ TRequestDictionary }

constructor TRequestsDictionary.Create;
begin
  FDict := TCollections.CreateDictionary<string, TValue>;
end;

destructor TRequestsDictionary.Destroy;
begin
  FDict := nil;
  inherited;
end;

function TRequestsDictionary.Get(AKey: string): TValue;
begin
  if not FDict.TryGetValue(AKey, Result) then
    Result := '';
end;

function TRequestsDictionary.GetEnumerable: IEnumerable<TPair<string, TValue>>;
begin
  Result := FDict;
end;

function TRequestsDictionary.GetHeaderByName(AName: string;
  AValue: TValue): IRequestsDictionary;
begin
  FDict.AddOrSetValue(AName, AValue);
  Result := Self;
end;

{ TParameters }

function TParameters.AsString: string;
var
  Tmp: string;
begin
  Tmp := '';
  Self.Enumerable.ForEach(
    procedure(const APair: TPair<string, TValue>)
    begin
      if Tmp<>'' then
        Tmp := Tmp + '&';
      Tmp := Tmp + APair.Key + '=' + APair.Value.ToString;
    end
  );
  Result := Tmp;
end;

function TParameters.GetHeaderByName(AName: string;
  AValue: TValue): IParameters;
begin
  Result := TParameters(inherited GetHeaderByName(AName, AValue));
end;


end.
